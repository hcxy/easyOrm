package com.lingyuting.orm;

public class OrmSqlException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = 382452519947453955L;

    public OrmSqlException(String message) {
        super(message);
    }

    public OrmSqlException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrmSqlException(Throwable cause) {
        super(cause);
    }
}
