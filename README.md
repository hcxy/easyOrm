##easyOrm
是一个基于DbUtils快速操作MySQL的工具类

##如何使用
* 首先添加DbUtils和MySQL的jar包
* 设置数据库的链接,默认会去调用DbManager里面的方法，根据自己实际情况修改
```java
    public Connection getConnection() {
        try {
            if (conn != null && !conn.isClosed()) {
                return conn;
            }
        } catch (SQLException e) {
        }

        String url = "jdbc:mysql://127.0.0.1:3306/test?characterEncoding=UTF-8";
        String jdbcDriver = "com.mysql.jdbc.Driver";
        String user = "root";
        String password = "root";
        DbUtils.loadDriver(jdbcDriver);
        try {
            return conn = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
            DbUtils.closeQuietly(conn);
            throw new RuntimeException(e);
        }
    }
```
* AR类的创建,该工具进行了简单的封装，使得model的建立不在依赖get和set方法以及xml的配置
举例一个User类
```java
	package easyOrm.test;

	import java.sql.Connection;
	import java.util.HashMap;
	import java.util.Map;

	import com.lingyuting.orm.ActiveRecord;
	import com.lingyuting.orm.DbManager;

	public class User extends ActiveRecord<User> {

		private static User instance = null;

		/**
		 * 在自己的方法中实现一个单例
		 * 
		 * @return
		 */
		public static User getInstance() {
			if (null == instance) {
				instance = new User();
			}
			return instance;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @override
		 * @return
		 */
		public Connection getDbConnection() {
			return DbManager.getInstance().getConnection();
		}

		/**
		 * {@inheritDoc}
		 */
		public String getTable() {
			return "user";
		}

		/**
		 * 最佳实践
		 * 
		 * @param args
		 */
		public static void main(String[] args) {
			Map<String, Object> change = new HashMap<String, Object>();
			change.put("nick", "xiaomi1");
			change.put("password", "xiaomi234");
			User.getInstance().updateAll(change, "id = ?", 1);
			User u = new User();
			u.setAttribute("nick", "easyOrm");
			u.setAttribute("username", "bennett");
			u.setAttribute("password", "bennett");
			u.save();
			System.out.println(u.getAttribute("id"));
			User.getInstance().findByPk("3");
			User.getInstance().find("nick = ?", "easyOrm");
			User.getInstance().findAll();
			User.getInstance().findAll("id > ?", 1);
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("nick", "bennett");
			User.getInstance().updateByPk("1", data);
			User model = User.getInstance().findByPk("1");
			model.setAttribute("nick", "cccc");
			model.update();// or u.save();
		}
	}
```


##程序说明
* 多线程未测试

##有问题反馈
在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我交流

* QQ: 893157632